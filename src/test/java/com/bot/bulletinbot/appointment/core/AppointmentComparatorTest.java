package com.bot.bulletinbot.appointment.core;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class AppointmentComparatorTest {

    private AppointmentComparator appointmentComparator = AppointmentComparator.getInstance();

    @Test
    void testCompareMethod() {
        Appointment appointment1 = new Appointment();
        Appointment appointment2 = new Appointment();
        appointment1.setAppointmentName("a");
        appointment2.setAppointmentName("b");
        Assertions.assertTrue(appointmentComparator.compare(appointment1, appointment2) < 0);

        appointment2.setAppointmentName("a");
        Assertions.assertTrue(appointmentComparator.compare(appointment1, appointment2) == 0);

        appointment1.setAppointmentName("b");
        Assertions.assertTrue(appointmentComparator.compare(appointment1, appointment2) > 0);
    }
}
