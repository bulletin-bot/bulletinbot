package com.bot.bulletinbot.appointment.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

class AppointmentPkIdTest {
    private Class<?> appointmentPkIdClass;
    private AppointmentPkId appointmentPkId;

    @BeforeEach
    public void setUp() throws Exception {
        appointmentPkIdClass = Class.forName("com.bot.bulletinbot.appointment.core.AppointmentPkId");
        appointmentPkId = new AppointmentPkId();
    }

    @Test
    void testAppointmentPkIdIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(appointmentPkIdClass.getModifiers()));
    }

    @Test
    void testAppointmentPkIdHasGetUserIdMethod() throws Exception {
        Method getUserId = appointmentPkIdClass.getDeclaredMethod("getUserId");

        assertEquals("java.lang.String",
                getUserId.getGenericReturnType().getTypeName());
        assertEquals(0,
                getUserId.getParameterCount());
        assertTrue(Modifier.isPublic(getUserId.getModifiers()));
    }

    @Test
    void testAppointmentPkIdGetUserIdReturnString() {
        appointmentPkId.setUserId("ay");
        assertEquals("ay", appointmentPkId.getUserId());
    }

    @Test
    void testAppointmentPkIdHasSetUserIdMethod() throws Exception {
        Method setUserId = appointmentPkIdClass.getDeclaredMethod("setUserId", String.class);

        assertEquals("void",
                setUserId.getGenericReturnType().getTypeName());
        assertEquals(1,
                setUserId.getParameterCount());
        assertTrue(Modifier.isPublic(setUserId.getModifiers()));
    }

    @Test
    void testAppointmentPkIdHasGetAppointmentIdMethod() throws Exception {
        Method getAppointmentId = appointmentPkIdClass.getDeclaredMethod("getAppointmentId");

        assertEquals("java.lang.Long",
                getAppointmentId.getGenericReturnType().getTypeName());
        assertEquals(0,
                getAppointmentId.getParameterCount());
        assertTrue(Modifier.isPublic(getAppointmentId.getModifiers()));
    }

    @Test
    void testAppointmentPkIdGetAppointmentIdReturnLong() {
        appointmentPkId.setAppointmentId(1L);
        assertEquals(1L, appointmentPkId.getAppointmentId());
    }


    @Test
    void testAppointmentPkIdEqualMethod() {
        appointmentPkId.setUserId("a");
        appointmentPkId.setAppointmentId(1L);

        AppointmentPkId appointmentId2 = appointmentPkId;
        assertEquals(appointmentPkId, appointmentId2);

        appointmentId2 = new AppointmentPkId("b", 1L);
        assertNotEquals(appointmentPkId, appointmentId2);
    }

    @Test
    void testAppointmentPkIdHashCodeMethod() throws Exception {
        Method hashCode = appointmentPkIdClass.getDeclaredMethod("hashCode");

        assertEquals("int",
                hashCode.getGenericReturnType().getTypeName());
        assertEquals(0,
                hashCode.getParameterCount());
        assertTrue(Modifier.isPublic(hashCode.getModifiers()));

        int hashcode = appointmentPkId.hashCode();
        assertEquals(hashcode, appointmentPkId.hashCode());
    }
}