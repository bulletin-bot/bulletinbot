package com.bot.bulletinbot.appointment.service;

import com.bot.bulletinbot.appointment.core.Appointment;
import com.bot.bulletinbot.appointment.repository.AppointmentRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;

import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class AppointmentServiceImplTest {
    @Mock
    private AppointmentRepository appointmentRepository;

    @InjectMocks
    private AppointmentServiceImpl appointmentService;

    private Appointment appointment;
    private String userId;

    @BeforeEach
    public void setUp(){
        appointment = new Appointment();
        appointment.setUserId("Aq");
        appointment.setAppointmentId((long) 1);
        appointment.setAppointmentName("Demo");
        appointment.setAppointmentPlace("meet.google.com/abc-defg-hijk");
        appointment.setAppointmentTime(LocalDateTime.now());
        appointment.setAppointmentDescription("Demo AP");
        userId = "Saya";
    }

    @Test
    void testServiceGetAllAppointment() {
        Iterable<Appointment> listAppointment = appointmentRepository.findAll();
        lenient().when(appointmentService.getAll()).thenReturn(listAppointment);
        Iterable<Appointment> listAppointmentResult = appointmentService.getAll();
        Assertions.assertIterableEquals(listAppointment, listAppointmentResult);
    }

    @Test
    void testServiceGetListAppointment() {
        Iterable<Appointment> listAppointment = appointmentRepository.findByUserId(userId);
        lenient().when(appointmentService.getListAppointment(userId)).thenReturn(listAppointment);
        Iterable<Appointment> listAppointmentResult = appointmentService.getListAppointment(userId);
        Assertions.assertIterableEquals(listAppointment, listAppointmentResult);
    }

    @Test
    void testServiceGetAppointment() {
        lenient().when(appointmentService.getAppointmentByAppointmentId(userId, appointment.getAppointmentId())).thenReturn(appointment);
        Appointment resultAppointment = appointmentService.getAppointmentByAppointmentId(userId, appointment.getAppointmentId());
        Assertions.assertEquals(appointment.getAppointmentId(), resultAppointment.getAppointmentId());
    }

    @Test
    void testServiceCreateAppointment() {
        lenient().when(appointmentService.createAppointment(appointment)).thenReturn(appointment);
        Appointment resultAppointment = appointmentService.createAppointment(appointment);
        Assertions.assertEquals(appointment.getAppointmentId(), resultAppointment.getAppointmentId());

        appointment = new Appointment(userId, "Ap", "Meet", LocalDateTime.now(), "Desc");
        lenient().when(appointmentService.createAppointment(appointment)).thenReturn(appointment);
        resultAppointment = appointmentService.createAppointment(appointment);
        Assertions.assertEquals(appointment.getAppointmentId(), resultAppointment.getAppointmentId());
    }


    @Test
    void testServiceUpdateAppointment() {
        appointmentService.createAppointment(appointment);
        String userId = "Saya";
        String appointmentName = "Rapat";
        String appointmentPlace = "Jakarta";
        LocalDateTime appointmentTime = LocalDateTime.now();
        String appointmentDescription = "Jkt";

        appointment.setUserId(userId);
        appointment.setAppointmentName(appointmentName);
        appointment.setAppointmentPlace(appointmentPlace);
        appointment.setAppointmentTime(appointmentTime);
        appointment.setAppointmentDescription(appointmentDescription);

        Appointment expectedAppointment = appointment;
        expectedAppointment.setUserId(userId);
        expectedAppointment.setAppointmentName(appointmentName);
        expectedAppointment.setAppointmentPlace(appointmentPlace);
        expectedAppointment.setAppointmentTime(appointmentTime);
        expectedAppointment.setAppointmentDescription(appointmentDescription);

        Appointment resultAppointment = appointmentService.updateAppointment(userId, appointment.getAppointmentId(), appointment);
        Assertions.assertEquals(expectedAppointment.getUserId(), resultAppointment.getUserId());
        Assertions.assertEquals(expectedAppointment.getAppointmentName(), resultAppointment.getAppointmentName());
        Assertions.assertEquals(expectedAppointment.getAppointmentPlace(), resultAppointment.getAppointmentPlace());
        Assertions.assertEquals(expectedAppointment.getAppointmentTime(), resultAppointment.getAppointmentTime());
        Assertions.assertEquals(expectedAppointment.getAppointmentDescription(), resultAppointment.getAppointmentDescription());
    }

    @Test
    void testServiceDeleteAppointment() {
        appointmentService.createAppointment(appointment);
        appointmentService.deleteAppointmentByAppointmentId(userId, appointment.getAppointmentId());
        Assertions.assertNull(appointmentService.getAppointmentByAppointmentId(userId, appointment.getAppointmentId()));
    }
}
