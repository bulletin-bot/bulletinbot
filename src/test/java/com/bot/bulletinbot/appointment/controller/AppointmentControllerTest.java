package com.bot.bulletinbot.appointment.controller;

import com.bot.bulletinbot.appointment.core.Appointment;
import com.bot.bulletinbot.appointment.service.AppointmentServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(controllers = AppointmentController.class)
class AppointmentControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private AppointmentServiceImpl appointmentService;

    private Appointment appointment;

    @BeforeEach
    public void setUp() {
        appointment = new Appointment();
        appointment.setUserId("Aq");
        appointment.setAppointmentId((long) 1);
        appointment.setAppointmentName("Demo");
        appointment.setAppointmentPlace("meet.google.com/abc-defg-hijk");
        appointment.setAppointmentTime(LocalDateTime.parse("30-01-2020 13:00", DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm")));
        appointment.setAppointmentDescription("Demo AP");
    }

    private String mapToJson(Appointment obj) throws JsonProcessingException {
        JSONObject request = new JSONObject();

        request.put("userId", obj.getUserId());
        request.put("appointmentName", obj.getAppointmentName());
        request.put("appointmentPlace", obj.getAppointmentPlace());
        request.put("appointmentTime", obj.getAppointmentTime());
        request.put("appointDescription", obj.getAppointmentDescription());
        return request.toString();
    }

    @Test
    void testControllerGetListAppointment() throws Exception {
        Iterable<Appointment> listAppointment = Arrays.asList(appointment);
        when(appointmentService.getAll()).thenReturn(listAppointment);

        mvc.perform(get("/appointment").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].appointmentName").value(appointment.getAppointmentName()));
    }

    @Test
    void testControllerGetListAppointmentUser() throws Exception {
        Iterable<Appointment> listAppointment = Arrays.asList(appointment);
        when(appointmentService.getListAppointment(appointment.getUserId())).thenReturn(listAppointment);

        mvc.perform(get("/appointment/user/" + appointment.getUserId()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].appointmentName").value(appointment.getAppointmentName()));
    }

    @Test
    void testControllerCreateAppointment() throws Exception {
        when(appointmentService.createAppointment(appointment)).thenReturn(appointment);

        mvc.perform(post("/appointment")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(appointment)));
    }

    @Test
    void testControllerGetAppointment() throws Exception {
        when(appointmentService.getAppointmentByAppointmentId(appointment.getUserId(), appointment.getAppointmentId())).thenReturn(appointment);
        mvc.perform(get("/appointment/user/" + appointment.getUserId() + "/" + appointment.getAppointmentId()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.appointmentName").value(appointment.getAppointmentName()));
    }

    @Test
    void testControllerGetAppointmentKosong() throws Exception {
        when(appointmentService.getAppointmentByAppointmentId("test", 2L)).thenReturn(null);
        mvc.perform(get("/appointment/user/test/2").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void testControllerUpdateAppointment() throws Exception {
        appointmentService.createAppointment(appointment);

        String namaAppointment = "ADV125YIHA";
        appointment.setAppointmentName(namaAppointment);

        when(appointmentService.updateAppointment(appointment.getUserId(), appointment.getAppointmentId(), appointment)).thenReturn(appointment);

        mvc.perform(put("/appointment/user/" + appointment.getUserId() + "/" + appointment.getAppointmentId()).contentType(MediaType.APPLICATION_JSON)
                .content(mapToJson(appointment)))
                .andExpect(status().isOk());
    }

    @Test
    void testControllerDeleteAppointment() throws Exception {
        appointmentService.createAppointment(appointment);
        mvc.perform(delete("/appointment/user/" + appointment.getUserId() + "/" + appointment.getAppointmentId()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

}

