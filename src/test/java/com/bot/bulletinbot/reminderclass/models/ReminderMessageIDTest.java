package com.bot.bulletinbot.reminderclass.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


@ExtendWith(MockitoExtension.class)
public class ReminderMessageIDTest {
    @InjectMocks
    ReminderMessageID reminderMessageID;

    @InjectMocks
    ReminderMessageID r2;

    @BeforeEach
    public void setUp(){
        reminderMessageID = new ReminderMessageID();
        reminderMessageID.setRemindId(1L);
        reminderMessageID.setUserid("user1");
        int remindHash = reminderMessageID.hashCode();
    }

    @Test
    void testReminderMessageIDEqualSelf(){
        r2 = reminderMessageID;
        assertTrue(reminderMessageID.equals(r2));
    }
    @Test
    void testReminderMessageIDEqualnull(){
        assertFalse(reminderMessageID.equals(null));
    }
}
