package com.bot.bulletinbot.reminderclass.models;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;

@ExtendWith(MockitoExtension.class)
public class ReminderMessageTest {
    @InjectMocks
    ReminderMessage reminderMessage;

    private ReminderMessageID reminderMessageID;

    @BeforeEach
    public void setUp(){
        reminderMessageID = new ReminderMessageID();
        reminderMessageID.setRemindId(1L);
        reminderMessageID.setUserid("user1");

        reminderMessage = new ReminderMessage();
        reminderMessage.setRemindId(reminderMessageID.getRemindId());
        reminderMessage.setUserid(reminderMessageID.getUserid());
        reminderMessage.setMessage("deadline adpro");
        reminderMessage.setState("once");
        Calendar calendar = Calendar.getInstance();
        calendar.set(2021,3,23,16,43,41);
        reminderMessage.setRemindTime(calendar.getTime());

        int remindHash = reminderMessageID.hashCode();
    }

    @Test
    void testReminderMessageCreatedCorrectly(){
        assertEquals(1, reminderMessage.getRemindId());
        assertEquals("user1",reminderMessage.getUserid());
        assertEquals("deadline adpro", reminderMessage.getMessage());
        assertEquals("once",reminderMessage.getState());
        Date date = reminderMessage.getRemindTime();
    }

    @Test
    void testReminderMessageCreated(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(2021,5,2,23,55);
        Date date=calendar.getTime();
        reminderMessage = new ReminderMessage(1, "user1", "deadline uwu", date,"once");
    }
}
