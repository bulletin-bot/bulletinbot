package com.bot.bulletinbot.todo.repository;

import com.bot.bulletinbot.todo.core.Todo;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class TodoRepositoryTest {
    @InjectMocks
    private TodoRepository todoRepository = new TodoRepository();

    @BeforeEach
    public void setUp() throws ParseException {
        todoRepository = new TodoRepository();
    }

    @Test
    public void testAddTodoUserBaru() {
        Todo newTodo = new Todo("todoBaru");
        String userIDNew = "UserIDNew";
        todoRepository.add(newTodo, userIDNew);
        assertEquals(todoRepository.getTodoHashMap().get(userIDNew).size(), 1);
    }

    @Test
    public void testAddTodoUserLama() {
        Todo newTodo = new Todo("todoBaru");
        String userIDNew = "UserIDNew";
        todoRepository.add(newTodo, userIDNew);

        Todo newTodo2 = new Todo("todoBaru2");
        todoRepository.add(newTodo2, userIDNew);

        assertEquals(todoRepository.getTodoHashMap().get(userIDNew).size(), 2);
    }
}
