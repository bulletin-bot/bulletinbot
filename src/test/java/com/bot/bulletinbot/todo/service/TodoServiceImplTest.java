package com.bot.bulletinbot.todo.service;


import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class TodoServiceImplTest {

    @InjectMocks
    private TodoServiceImpl todoServiceImpl = new TodoServiceImpl();

    @BeforeEach
    public void setup() throws Exception {
        todoServiceImpl = new TodoServiceImpl();
    }

    @Test
    public void testInitRepoHasBeenInitialized() throws Exception {
        assertEquals(1, todoServiceImpl.getTodoHashMap().get("DefaultID").size());
    }

    @Test
    public void testCreateTodoMethod() {
        int oldsize = todoServiceImpl.getTodoHashMap().get("DefaultID").size();
        todoServiceImpl.createTodo("newTest", "DefaultID");
        assertEquals(oldsize+1, todoServiceImpl.getTodoHashMap().get("DefaultID").size());
    }


}
