package com.bot.bulletinbot.todo.core;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class TodoCoreTest {

    @InjectMocks
    private Todo todo = new Todo("todoTest");

    @BeforeEach
    public void setUp() throws ParseException {
        todo = new Todo("todoTest");
    }

    @Test
    public void testGetName() {
        assertEquals("todoTest", todo.getName());
    }

    @Test
    public void testSetName() {
        todo.setName("updateTodo");
        assertEquals("updateTodo", todo.getName());
    }
}
