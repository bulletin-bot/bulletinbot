package com.bot.bulletinbot.bulletinbot.controller;


import com.bot.bulletinbot.bulletinbot.service.ReminderBotService;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.source.UserSource;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Instant;

@ExtendWith(MockitoExtension.class)
public class BotControllerTest {
    @Mock
    LineMessagingClient lineMessagingClient;

    @Mock
    ReminderBotService reminderBotService;

    @InjectMocks
    BotController botController;

    @Test
    void handleTextEventTestReminder() throws Exception{
        MessageEvent<TextMessageContent> request = new MessageEvent<>(
                "replyToken",
                new UserSource("userId"),
                new TextMessageContent("id", "/reminder help"),
                Instant.now()
        );
        botController.handleTextEvent(request);

        request = new MessageEvent<>(
                "replyToken",
                new UserSource("userId"),
                new TextMessageContent("id", "/reminder create 23-05-2021 18:17 remind2"),
                Instant.now()
        );
        botController.handleTextEvent(request);

        request = new MessageEvent<>(
                "replyToken",
                new UserSource("userId"),
                new TextMessageContent("id", "/reminder all"),
                Instant.now()
        );
        botController.handleTextEvent(request);

        request = new MessageEvent<>(
                "replyToken",
                new UserSource("userId"),
                new TextMessageContent("id", "/reminder id 1"),
                Instant.now()
        );
        botController.handleTextEvent(request);

        request = new MessageEvent<>(
                "replyToken",
                new UserSource("userId"),
                new TextMessageContent("id", "/reminder update 1 23-05-2021 18:17 remindx"),
                Instant.now()
        );
        botController.handleTextEvent(request);

        request = new MessageEvent<>(
                "replyToken",
                new UserSource("userId"),
                new TextMessageContent("id", "/reminder delete 1"),
                Instant.now()
        );
        botController.handleTextEvent(request);
    }

    @Test
    void handleTextEventTestTodo() throws Exception{
        MessageEvent<TextMessageContent> request = new MessageEvent<>(
                "replyToken",
                new UserSource("userId"),
                new TextMessageContent("id", "/todo help"),
                Instant.now()
        );
        botController.handleTextEvent(request);

        request = new MessageEvent<>(
                "replyToken",
                new UserSource("userId"),
                new TextMessageContent("id", "/todo list"),
                Instant.now()
        );
        botController.handleTextEvent(request);

        request = new MessageEvent<>(
                "replyToken",
                new UserSource("userId"),
                new TextMessageContent("id", "/todo add belikentang"),
                Instant.now()
        );
        botController.handleTextEvent(request);

        request = new MessageEvent<>(
                "replyToken",
                new UserSource("userId"),
                new TextMessageContent("id", "/todo edit belikentang : makanayam"),
                Instant.now()
        );
        botController.handleTextEvent(request);

        request = new MessageEvent<>(
                "replyToken",
                new UserSource("userId"),
                new TextMessageContent("id", "/todo delete makanayam"),
                Instant.now()
        );
        botController.handleTextEvent(request);

    }

    @Test
    void handleTextEventTestAppointment() throws Exception{
        MessageEvent<TextMessageContent> request = new MessageEvent<>(
                "replyToken",
                new UserSource("userId"),
                new TextMessageContent("id", "/appointment help"),
                Instant.now()
        );
        botController.handleTextEvent(request);

        request = new MessageEvent<>(
                "replyToken",
                new UserSource("userId"),
                new TextMessageContent("id", "/appointment test"),
                Instant.now()
        );
        botController.handleTextEvent(request);

        request = new MessageEvent<>(
                "replyToken",
                new UserSource("userId"),
                new TextMessageContent("id", "/appointment list"),
                Instant.now()
        );
        botController.handleTextEvent(request);

        request = new MessageEvent<>(
                "replyToken",
                new UserSource("userId"),
                new TextMessageContent("id", "/appointment show"),
                Instant.now()
        );
        botController.handleTextEvent(request);

        request = new MessageEvent<>(
                "replyToken",
                new UserSource("userId"),
                new TextMessageContent("id", "/appointment add"),
                Instant.now()
        );
        botController.handleTextEvent(request);


        request = new MessageEvent<>(
                "replyToken",
                new UserSource("userId"),
                new TextMessageContent("id", "/appointment add @name Demo AP @place meet.google.com/abc @time 30-01-2020 13:00 @desc desc"),
                Instant.now()
        );
        botController.handleTextEvent(request);

        request = new MessageEvent<>(
                "replyToken",
                new UserSource("userId"),
                new TextMessageContent("id", "/appointment show 1"),
                Instant.now()
        );
        botController.handleTextEvent(request);

        request = new MessageEvent<>(
                "replyToken",
                new UserSource("userId"),
                new TextMessageContent("id", "/appointment update"),
                Instant.now()
        );
        botController.handleTextEvent(request);

        request = new MessageEvent<>(
                "replyToken",
                new UserSource("userId"),
                new TextMessageContent("id", "/appointment update @id userId @name Demo PA @place meet.google.com/abc @time 30-01-2020 13:00 @desc desc"),
                Instant.now()
        );
        botController.handleTextEvent(request);

        request = new MessageEvent<>(
                "replyToken",
                new UserSource("userId"),
                new TextMessageContent("id", "/appointment delete"),
                Instant.now()
        );
        botController.handleTextEvent(request);

        request = new MessageEvent<>(
                "replyToken",
                new UserSource("userId"),
                new TextMessageContent("id", "/appointment delete 1"),
                Instant.now()
        );
        botController.handleTextEvent(request);
    }
}
