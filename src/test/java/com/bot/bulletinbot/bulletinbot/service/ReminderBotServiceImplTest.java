package com.bot.bulletinbot.bulletinbot.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

@ExtendWith(MockitoExtension.class)
public class ReminderBotServiceImplTest {

    @InjectMocks
    ReminderBotServiceImpl reminderBotService;
    @Test
    void testServiceCreateReminderMessage(){
        Date now = new Date();
        String ans = reminderBotService.createReminder(1L,"user1",now,"ini reminder");
        assertNotEquals(ans,null);
        assertTrue(ans.contains("ini reminder"));
    }

    @Test
    void testServiceGetReminderByID(){
        Date now = new Date();
        reminderBotService.createReminder(2L,"user1",now,"ini reminder2");
        String ans = reminderBotService.getReminderByID(2L,"user1");
        assertNotEquals(ans,null);
        assertTrue(ans.contains("reminder2"));
    }

    @Test
    void testServiceGetReminderByUser(){
        Date now = new Date();
        reminderBotService.createReminder(4L,"user1",now,"ini reminder4");
        String ans = reminderBotService.getReminderByUser("user1");
        assertNotEquals(ans,null);
        assertTrue(ans.contains("reminder4"));
    }

    @Test
    void testServiceUpdateReminder(){
        Date now = new Date();
        reminderBotService.createReminder(3L,"user1",now,"ini reminder3");
        String ans = reminderBotService.updateReminder(3L,"user1",now,"updated reminder");
        assertNotEquals(ans,null);
        assertTrue(ans.contains("updated"));
    }
}
