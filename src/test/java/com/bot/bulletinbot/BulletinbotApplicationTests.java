package com.bot.bulletinbot;

import com.bot.bulletinbot.bulletinbot.controller.BotController;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
class BulletinbotApplicationTests {

	@Autowired
	private BotController botController;

	@Test
	void contextLoads() {
		assertThat(botController).isNotNull();
	}

}
