package com.bot.bulletinbot.bulletinbot.service;

import java.util.Date;

public interface ReminderBotService {
    String createReminder(long id, String userid, Date date, String subject);
    String getReminderByUser(String userid);
    String getReminderByID(long id, String userid);
    String updateReminder(long id, String userid, Date date, String subject);
    String deleteReminderById(long id, String userid);
}
