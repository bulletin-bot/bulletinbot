package com.bot.bulletinbot.bulletinbot.controller;


import com.bot.bulletinbot.appointment.core.Appointment;
import com.bot.bulletinbot.appointment.service.AppointmentService;
import com.bot.bulletinbot.bulletinbot.service.ReminderBotService;
import com.bot.bulletinbot.todo.core.Todo;
import com.bot.bulletinbot.todo.service.TodoServiceImpl;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;


@RestController
@LineMessageHandler
public class BotController {
    @Autowired
    private LineMessagingClient lineMessagingClient;

    @Autowired
    private ReminderBotService reminderBotService;

    @Autowired
    private TodoServiceImpl todoService = new TodoServiceImpl();

    @Autowired
    private AppointmentService appointmentService;

    private static final String TIMEFORMAT = "dd-MM-yyyy HH:mm";
    private static final String URL =
            "https://linebot-appointment-bulletin.herokuapp.com/appointment/";

    RestTemplate restTemplate = new RestTemplate();
    private long counter=0;
    private String userId;

    @EventMapping
    public void handleTextEvent(MessageEvent<TextMessageContent> messageEvent) throws ParseException, InterruptedException, ExecutionException {
        userId = messageEvent.getSource().getUserId();
        String message = messageEvent.getMessage().getText().toLowerCase();
        String[] splitInput = message.split(" ");
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        String userName = "";
        List<Todo> todoList = null;
        try {
            userName = lineMessagingClient.getProfile(userId).get().getDisplayName();
            todoList = todoService.getTodoHashMap().get(userId);
        }catch (NullPointerException e){
            System.out.println(e);
        }
        // Default Response
        String answer =" Halo! ketik '/todo help', '/reminder help', ataupun '/appointment help' untuk melihat daftar command yang ada";
        if(message.contains("/reminder")) {
            if (message.contains("/reminder help")) {
                answer = "'/reminder create [date(dd-MM-yyyy hh:mm)] [subject]’, \n " +
                        "'/reminder all',\n" +
                        "'/reminder id [id]',\n" +
                        "'/reminder update [id] [date(dd-MM-yyyy hh:mm)] [subject',\n" +
                        "'/reminder delete [id]'";

            } else if (message.contains("/reminder create ")) {
                String strDate = splitInput[2] + " " + splitInput[3];
                Date date = formatter.parse(strDate);
                String rawMessage = message.replaceAll("/reminder create" + " " + strDate + " ", "");

                answer = reminderBotService.createReminder(++counter, userId, date, rawMessage);

            } else if (message.contains("/reminder all")) {
                answer = reminderBotService.getReminderByUser(userId);

            } else if (message.contains("/reminder id ")) {
                answer = reminderBotService.getReminderByID(Long.parseLong(splitInput[2]), userId);

            } else if (message.contains("/reminder update ")) {
                String strDate = splitInput[3] + " " + splitInput[4];
                Date date = formatter.parse(strDate);
                String rawMessage = message.replaceAll("/reminder update" + " " + splitInput[2] + " " + strDate + " ", "");
                answer = reminderBotService.updateReminder(Long.parseLong(splitInput[2]), userId, date, rawMessage);

            } else if (message.contains("/reminder delete ")) {
                answer = reminderBotService.deleteReminderById(Long.parseLong(splitInput[2]), userId);

            }
        }
        else if (message.contains("/todo")){
            if (message.contains("hello")) {
                answer = ("Hello " + userName);

            } else if (message.contains("/todo help")) {
                answer = ("Daftar command yang tersedia: \n1. /todo list \n2. /todo add 'todo'\n3. /todo edit 'todo1':'todo2'\n4. /todo delete 'todo'");


            } else if (message.contains("list")) {
                if (todoList == null) {
                    answer = ("Belum ada Todo untuk ditampilkan");
                }
                else {
                    todoList = todoService.getTodoHashMap().get(userId);
                    String output = "";
                    int counter = 1;
                    for (Todo i : todoList) {
                        output += counter + ". " + i.getName() + "\n";
                        counter += 1;
                    }
                    answer = ("Todo yang ada adalah: \n" + output);
                }
            } else if (message.contains("add")) {
                String todoName = message.substring(10);
                if (todoList == null) {
                    todoService.createTodo(todoName, userId);
                    answer = (todoName + " berhasil ditambahkan");
                }
                else {
                    boolean exist = false;
                    for (Todo todo : todoList) {
                        if (todo.getName().equalsIgnoreCase(todoName)) {
                            exist = true;
                        }
                    }
                    if (exist) {
                        answer = ("Sudah terdapat todo " + todoName);
                    }
                    else {
                        todoService.createTodo(todoName, userId);
                        answer = (todoName + " berhasil ditambahkan");
                    }
                }


            } else if (message.contains("edit")) {
                String todoName = message.substring(11);
                String[] pisah = todoName.split(":");
                String todo1 = pisah[0];
                String todo2 = pisah[1];
                todoList = todoService.getTodoHashMap().get(userId);
                boolean found = false;

                for (Todo i : todoList) {
                    if (i.getName().equals(todo1)) {
                        i.setName(todo2);
                        found = true;
                        break;
                    }
                }
                if (found) {
                    answer = (todo1 + " berhasil diganti menjadi " + todo2);
                }
                else {
                    answer = ("Tidak ada " + todo1 + " dalam Todo List");
                }

            } else if (message.contains("delete")) {
                String todoName = message.substring(13);
                todoList = todoService.getTodoHashMap().get(userId);
                boolean found = false;

                for (Todo i : todoList) {
                    if (i.getName().equals(todoName)) {
                        todoList.remove(i);
                        found = true;
                        break;
                    }
                }
                if (found) {
                    answer = (todoName + " berhasil dihapus");
                }
                else {
                    answer = ("Tidak ada " + todoName + " dalam Todo List");
                }


            } else if (message.contains("debug")) {
                answer = ("oke 1 2 3");

            }


        }
        else if (message.contains("/appointment")){
            if (message.contains("/appointment help")) {
                answer = "APPOINTMENT COMMAND\n\n" +
                        "-> /appointment list\n" +
                        "-> /appointment show [id]\n" +
                        "-> /appointment add @name [name] @place [place] @time [dd-MM-yyy HH:mm] @desc [desc]\n" +
                        "-> /appointment edit @id [id] @name [name] @place [place] @time [dd-MM-yyy HH:mm] @desc [desc]\n" +
                        "-> /appointment delete [id]\n";

            }else{
                String command = splitInput[1].toLowerCase();
                System.out.println(command);
                answer = executeCommand(command, splitInput);
            }
        }
        String replyToken = messageEvent.getReplyToken();
        handleReplyEvent(replyToken, answer);

    }

    /**
     * Handle reply message.
     *
     * @param replyToken Token as User identifier.
     * @param answer     Message that will be send to User.
     */
    public void handleReplyEvent(String replyToken, String answer) {
        TextMessage answerInTextMessage = new TextMessage(answer);
        try {
            lineMessagingClient
                    .replyMessage(new ReplyMessage(replyToken, answerInTextMessage))
                    .get();
        } catch (NullPointerException | InterruptedException | ExecutionException e) {
            System.out.print("ERROR: " + e.getMessage() + "\n");
        }
    }

    private String executeCommand(String command, String[] messageSplit) {
        switch (command) {
            case ("test"):
                return "Test succeed\n" + userId;
            case ("list"):
                return listAppointment();
            case ("show"):
                return showAppointment(messageSplit);
            case ("add"):
                return addAppointment(messageSplit);
            case ("update"):
                return updateAppointment(messageSplit);
            case ("delete"):
                return deleteAppointment(messageSplit);
            default:
                return "Unknown command, try type /appointment";
        }
    }

    private String listAppointment() {
        String answer;

        try {
            Appointment[] appointments = restTemplate.getForObject(
                    URL + "user/" + userId,
                    Appointment[].class
            );

            answer = "LIST APPOINTMENT - " + appointments.length;

            for (Appointment app : appointments) {
                answer =
                        answer.concat(
                                "\n[" + app.getAppointmentId() + "] " + app.getAppointmentName()
                        );
            }

            return answer;
        } catch (Exception e) {
            return "Something unexpected has occurred\n\n" + e.toString();
        }
    }

    private String showAppointment(String[] messages) {
        String answer;

        if (messages.length == 2) {
            answer =
                    "/appointment show [appointment id]\n\n" +
                            "Example:\n" +
                            "/appointment show 123";
        } else {
            String[] text = Arrays.copyOfRange(messages, 2, messages.length);

            try {
                Appointment appointment = restTemplate.getForObject(
                        URL + "user/" + userId + "/" + text[0],
                        Appointment.class
                );

                answer =
                        "DETAIL APPOINTMENT [" + appointment.getAppointmentId() + "]\n";
                answer =
                        answer
                                .concat("\n\uD83D\uDD16 " + appointment.getAppointmentName())
                                .concat("\n\uD83C\uDFE0 " + appointment.getAppointmentPlace())
                                .concat(
                                        "\n\uD83D\uDDD3️ " +
                                                appointment
                                                        .getAppointmentTime()
                                                        .format(DateTimeFormatter.ofPattern(TIMEFORMAT))
                                )
                                .concat("\n✏️ " + appointment.getAppointmentDescription());
            } catch (Exception e) {
                answer = "There is no such Appointment";
            }
        }
        return answer;
    }

    private String addAppointment(String[] messages) {
        String answer;

        if (messages.length == 2) {
            answer =
                    "/appointment add @name [name] @place [place] " +
                            "@time [time (dd-MM-yyy HH:mm)] @desc [desc]\n" +
                            "Empty string to mark the field empty\n\n" +
                            "Example (@desc is empty):\n" +
                            "/appointment add @name Demo AP @place meet.google.com/abc @time 30-01-2020 13:00 @desc";
        } else {
            try {
                String[] text = String
                        .join(" ", Arrays.copyOfRange(messages, 2, messages.length))
                        .split(" @name | @place | @time | @desc ");

                // create request body
                JSONObject request = new JSONObject();

                request.put("userId", userId);
                request.put("appointmentName", text[0].replace("@name ", ""));
                request.put("appointmentPlace", text[1]);
                request.put("appointmentTime",
                        LocalDateTime.parse(text[2], DateTimeFormatter.ofPattern(TIMEFORMAT))
                );
                request.put("appointDescription", text[3]);

                // set headers and send request
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_JSON);
                HttpEntity<String> entity = new HttpEntity<>(
                        request.toString(), headers);

                ResponseEntity<Appointment> postResponse = restTemplate.exchange(
                        URL, HttpMethod.POST, entity, Appointment.class );

                if (postResponse.getStatusCode() == HttpStatus.OK) {
                    answer = "Appointment " + text[0].replace("@name ", "") + " is set!";
                } else {
                    answer = "Something is wrong";
                }
            } catch (Exception e) {
                answer = "Something unexpected has occurred\n\n" + e.toString();
            }
        }
        return answer;
    }

    private String updateAppointment(String[] messages) {
        String answer;

        if (messages.length == 2) {
            answer =
                    "/appointment update @id [appointment id] @name [name] @place [place] " +
                            "@time [time (dd-MM-yyy HH:mm)] @desc [desc]\n" +
                            "Example:\n" +
                            "/appointment update" +
                            "@id 500505 @name Demo AP @place meet.google.com/abc @time 30-01-2020 13:00 @desc desc";
        } else {
            try {
                String[] text = String
                        .join(" ", Arrays.copyOfRange(messages, 2, messages.length))
                        .split(" @id | @name | @place | @time | @desc ");

                // create request body
                JSONObject request = new JSONObject();

                request.put("appointmentId", text[0].replace("@id ", ""));
                request.put("userId", userId);
                request.put("appointmentPlace", text[2]);
                request.put("appointmentTime", LocalDateTime.parse(text[3], DateTimeFormatter.ofPattern(TIMEFORMAT)));
                request.put("appointDescription", text[4]);

                // set headers and send request
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_JSON);
                HttpEntity<String> entity = new HttpEntity<>(
                        request.toString(),
                        headers
                );
                ResponseEntity<Appointment> putResponse = restTemplate.exchange(
                        URL + "user/" + userId + "/" + text[0].replace("@id ", ""),
                        HttpMethod.PUT,
                        entity,
                        Appointment.class
                );

                if (putResponse.getStatusCode() == HttpStatus.OK) {
                    answer = "Appointment is updated!";
                } else {
                    answer = "Something is wrong";
                }
            } catch (Exception e) {
                answer = "Something unexpected has occurred\n\n" + e.toString();
            }
        }
        return answer;
    }

    private String deleteAppointment(String[] messages) {
        String answer;

        if (messages.length == 2) {
            answer =
                    "Need appointment ID to delete.\n\n" +
                            "Example:\n" +
                            "/appointment delete 123";
        } else {
            try {
                String[] text = Arrays.copyOfRange(messages, 2, messages.length);
                Map<String, String> params = new HashMap<>();
                params.put("userId", userId);
                params.put("appointmentId", text[0]);

                restTemplate.delete(URL + "user/{userId}/{appointmentId}", params);
                answer = String.format("Appointment #%s is deleted!", text[0]);
            } catch (Exception e) {
                answer = "Something unexpected has occurred\n\n" + e.toString();
            }
        }

        return answer;
    }
}
