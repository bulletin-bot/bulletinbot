package com.bot.bulletinbot.reminderclass.models;


import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class ReminderMessageID implements Serializable {

    private long remindId;
    private String userid;

    public Long getRemindId() {
        return remindId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReminderMessageID that = (ReminderMessageID) o;
        return remindId == that.remindId && userid.equals(that.userid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(remindId, userid);
    }

    public void setRemindId(Long remindId) {
        this.remindId = remindId;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }
}
