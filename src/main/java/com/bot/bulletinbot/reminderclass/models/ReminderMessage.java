package com.bot.bulletinbot.reminderclass.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "reminder_message")
@Data
@IdClass(ReminderMessageID.class)
@NoArgsConstructor
public class ReminderMessage implements Serializable {
    @Id
    @Column(name = "remind_id", nullable = false)
    private long remindId;

    @Id
    @Column(name = "userid", nullable = false)
    private String userid;

    @Column(name = "message")
    private String message;

    @Column(name = "remind_time")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private Date remindTime;

    @Column(name = "state")
    private String state;

    public ReminderMessage(long remindId, String userid, String message, Date remindTime, String state) {
        this.remindId = remindId;
        this.userid = userid;
        this.message = message;
        this.remindTime = remindTime;
        this.state = state;
    }
}
