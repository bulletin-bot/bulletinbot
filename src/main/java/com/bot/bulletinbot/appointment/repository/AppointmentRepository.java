package com.bot.bulletinbot.appointment.repository;

import com.bot.bulletinbot.appointment.core.Appointment;
import com.bot.bulletinbot.appointment.core.AppointmentPkId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AppointmentRepository extends JpaRepository<Appointment, AppointmentPkId> {
	@Query(value = "Select * FROM appointment WHERE user_id=?1 and appointment_id=?2", nativeQuery = true)
	Appointment findByAppointmentId(String userId, Long appointmentId);

	@Query(value = "Select * FROM appointment WHERE user_id=?1", nativeQuery = true)
	Iterable<Appointment> findByUserId(String userId);

	@Query(value = "Delete FROM appointment WHERE appointment_id=?1", nativeQuery = true)
	void deleteByAppointmentId(Long appointmentId);
}