package com.bot.bulletinbot.appointment.service;

import com.bot.bulletinbot.appointment.core.Appointment;
import com.bot.bulletinbot.appointment.core.AppointmentComparator;
import com.bot.bulletinbot.appointment.repository.AppointmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class AppointmentServiceImpl implements AppointmentService {

	@Autowired
	private AppointmentRepository appointmentRepository;

	@Override
	public Appointment createAppointment(Appointment appointment) {
		appointmentRepository.save(appointment);
		return appointment;
	}

	@Override
	public Iterable<Appointment> getAll() {
		return appointmentRepository.findAll();
	}

	@Override
	public Iterable<Appointment> getListAppointment(String userId) {
		AppointmentComparator appointmentComparator = AppointmentComparator.getInstance();
		Iterable<Appointment> appointments = appointmentRepository.findByUserId(userId);
		List<Appointment> listAppointment = new ArrayList<>();

		appointments.forEach(listAppointment::add);
		Collections.sort(listAppointment, appointmentComparator);

		return listAppointment;
	}

	@Override
	public Appointment getAppointmentByAppointmentId(
			String userId,
			Long appointmentId
	) {
		return appointmentRepository.findByAppointmentId(userId, appointmentId);
	}

	@Override
	public Appointment updateAppointment(
			String userId,
			Long appointmentId,
			Appointment appointment
	) {
		appointment.setAppointmentId(appointmentId);
		appointmentRepository.save(appointment);
		return appointment;
	}

	@Override
	public void deleteAppointmentByAppointmentId(
			String userId,
			Long appointmentId
	) {
		Appointment appointment = this.getAppointmentByAppointmentId(userId, appointmentId);
		appointmentRepository.delete(appointment);
	}
}