package com.bot.bulletinbot.appointment.service;

import com.bot.bulletinbot.appointment.core.Appointment;

public interface AppointmentService {
	Appointment createAppointment(Appointment appointment);

	Iterable<Appointment> getAll();

	Iterable<Appointment> getListAppointment(String userId);

	Appointment getAppointmentByAppointmentId(String userId, Long appointmentId);

	Appointment updateAppointment(String userId, Long appointmentId, Appointment appointment);

	void deleteAppointmentByAppointmentId(String userId, Long appointmentId);
}