package com.bot.bulletinbot.appointment.controller;


import com.bot.bulletinbot.appointment.core.Appointment;
import com.bot.bulletinbot.appointment.service.AppointmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/appointment")
public class AppointmentController {

    @Autowired
    private AppointmentService appointmentService;

    @PostMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Appointment> postAppointment(
            @RequestBody Appointment appointment
    ) {
        return ResponseEntity.ok(appointmentService.createAppointment(appointment));
    }

    @GetMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity <Iterable<Appointment>> getAll() {
        return ResponseEntity.ok(appointmentService.getAll());
    }

    @GetMapping(
            path = "/user/{userId}",
            produces = {"application/json"})
    @ResponseBody
    public ResponseEntity <Iterable<Appointment>> getAppointment(
            @PathVariable(value = "userId") String userId
    ) {
        return ResponseEntity.ok(appointmentService.getListAppointment(userId));
    }

    @GetMapping(
            path = "/user/{userId}/{appointmentId}",
            produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Appointment> getAppointment(
            @PathVariable(value = "userId") String userId,
            @PathVariable(value = "appointmentId") Long appointmentId
    )throws Exception {
        Appointment appointment = appointmentService.getAppointmentByAppointmentId(userId, appointmentId);
        if (appointment == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(appointment);
    }

    @PutMapping(
            path = "/user/{userId}/{appointmentId}",
            produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Appointment> updateAppointment(
            @PathVariable(value = "userId") String userId,
            @PathVariable(value = "appointmentId") Long appointmentId,
            @RequestBody Appointment appointment
    ) {
        return ResponseEntity.ok(appointmentService.updateAppointment(userId, appointmentId, appointment));
    }

    @DeleteMapping(
            path = "/user/{userId}/{appointmentId}",
            produces = {"application/json"})
    public ResponseEntity<HttpStatus> deleteAppointment(
            @PathVariable(value = "userId") String userId,
            @PathVariable(value = "appointmentId") Long appointmentId
    ) {
        appointmentService.deleteAppointmentByAppointmentId(userId, appointmentId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
