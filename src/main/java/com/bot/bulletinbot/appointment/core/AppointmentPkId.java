package com.bot.bulletinbot.appointment.core;

import java.io.Serializable;
import java.util.Objects;

public class AppointmentPkId implements Serializable {
    private String userId;

    private Long appointmentId;

    public AppointmentPkId() {}

    public AppointmentPkId(String userId, Long appointmentId) {
        this.userId = userId;
        this.appointmentId = appointmentId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(Long appointmentId) {
        this.appointmentId = appointmentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Appointment)) return false;
        Appointment appointment = (Appointment) o;
        return Objects.equals(getAppointmentId(), appointment.getAppointmentId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAppointmentId());
    }
}