package com.bot.bulletinbot.appointment.core;

import java.util.Comparator;

public class AppointmentComparator implements Comparator<Appointment> {
    private static AppointmentComparator self = null;

    @Override
    public int compare(Appointment o1, Appointment o2) {
        return o1.getAppointmentName().compareToIgnoreCase(o2.getAppointmentName());
    }

    public static AppointmentComparator getInstance() {
        if (self == null){
            self = new AppointmentComparator();
        }
        return self;
    }
}
