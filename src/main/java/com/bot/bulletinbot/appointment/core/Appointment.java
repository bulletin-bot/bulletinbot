package com.bot.bulletinbot.appointment.core;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name= "appointment")
@Data
@IdClass(AppointmentPkId.class)
public class Appointment implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "appointmentId")
	private Long appointmentId;

	@Id
	@Column(name = "userId")
	private String userId;

	@Column(name = "appointmentName")
	private String appointmentName;

	@Column(name = "appointmentPlace")
	private String appointmentPlace;

	@Column(name = "appointmentTime")
	private LocalDateTime appointmentTime;

	@Column(name = "appointmentDescription")
	private String appointmentDescription;

	public Appointment() {}

	public Appointment(String userId, String appointmentName, String appointmentPlace, LocalDateTime appointmentTime, String appointmentDescription) {
		this.userId = userId;
		this.appointmentName = appointmentName;
		this.appointmentPlace = appointmentPlace;
		this.appointmentTime = appointmentTime;
		this.appointmentDescription = appointmentDescription;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getAppointmentId() {
		return appointmentId;
	}

	public void setAppointmentId(Long appointmentId) {
		this.appointmentId = appointmentId;
	}

	public String getAppointmentName() {
		return appointmentName;
	}

	public void setAppointmentName(String appointmentName) {
		this.appointmentName = appointmentName;
	}

	public String getAppointmentPlace() {
		return appointmentPlace;
	}

	public void setAppointmentPlace(String appointmentPlace) {
		this.appointmentPlace = appointmentPlace;
	}

	public LocalDateTime getAppointmentTime() {
		return appointmentTime;
	}

	public void setAppointmentTime(LocalDateTime appointmentTime) {
		this.appointmentTime = appointmentTime;
	}

	public String getAppointmentDescription() {
		return appointmentDescription;
	}

	public void setAppointmentDescription(String appointmentDescription) {
		this.appointmentDescription = appointmentDescription;
	}
}