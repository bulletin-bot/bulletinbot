package com.bot.bulletinbot.todo.repository;

import com.bot.bulletinbot.todo.core.Todo;
import org.springframework.stereotype.Repository;

import java.util.AbstractList;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;

@Repository
public class TodoRepository {
    private List<Todo> list;
    private HashMap<String, List> todoHashMap;

    public TodoRepository() {
        this.list = new ArrayList<>();
        this.todoHashMap = new HashMap<String, List>();
    }

    public List<Todo> getTodoList() {
        return list;
    }

    public HashMap<String, List> getTodoHashMap() {
        return todoHashMap;
    }

    public Todo add(Todo todo, String userID) {
        if (todoHashMap.containsKey(userID)) {
            todoHashMap.get(userID).add(todo);
            return todo;
        }
        else {
            List<Todo> listBaru = new ArrayList<>();
            listBaru.add(todo);
            todoHashMap.put(userID, listBaru);
            return  todo;
        }
    }
}
