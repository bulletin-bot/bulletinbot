package com.bot.bulletinbot.todo.core;

public class Todo {
    private String name;

    public Todo(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String nama) {
        this.name = nama;
    }
}
