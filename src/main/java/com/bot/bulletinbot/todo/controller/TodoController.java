//package com.bot.bulletinbot.todo.controller;
//
//import com.bot.bulletinbot.todo.core.Todo;
//import com.bot.bulletinbot.todo.service.TodoService;
//import com.linecorp.bot.client.LineMessagingClient;
//import com.linecorp.bot.model.ReplyMessage;
//import com.linecorp.bot.model.event.MessageEvent;
//import com.linecorp.bot.model.event.message.TextMessageContent;
//import com.linecorp.bot.model.message.TextMessage;
//import com.linecorp.bot.spring.boot.annotation.EventMapping;
//import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.text.ParseException;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.concurrent.ExecutionException;
//
//@RestController
//@LineMessageHandler
//public class TodoController {
//    @Autowired
//    private LineMessagingClient lineMessagingClient;
//
//    @Autowired
//    private TodoService todoService;
//
//    @EventMapping
//    public void handleTextEvent(MessageEvent<TextMessageContent> messageEvent)
//            throws InterruptedException, ExecutionException {
//        try {
//            String pesan = messageEvent.getMessage().getText().toLowerCase();
//            String replyToken = messageEvent.getReplyToken();
//            String userId = messageEvent.getSource().getUserId();
//            String userName = lineMessagingClient.getProfile(userId).get().getDisplayName();
//
//            // Default Response
//            TextMessage jawab = new TextMessage("Halo! ketik 'help' untuk melihat daftar command yang ada");
//
//            if (pesan.contains("hello")) {
//                jawab = new TextMessage("Hello " + userName);
//
//            } else if (pesan.contains("/todo help")) {
//                jawab = new TextMessage("Daftar command yang tersedia: \n1. list \n2. add 'todo'\n3. edit 'todo1' 'todo2'\n4. delete 'todo'");
//
//
//            } else if (pesan.contains("/todo list")) {
//                List<Todo> todoList = todoService.getTodoHashMap().get(userId);
//                String output = "";
//                int counter = 1;
//                for (Todo i : todoList) {
//                    output += counter + ". " + i.getName() + "\n";
//                    counter += 1;
//                }
//                jawab = new TextMessage("Todo yang ada adalah: \n" + output);
//
//            } else if (pesan.contains("/todo add")) {
//                String todoName = pesan.substring(4);
//                todoService.createTodo(todoName, userId);
//                jawab = new TextMessage(todoName + " Berhasil ditambahkan");
//
//            } else if (pesan.contains("/todo edit")) {
//                jawab = new TextMessage("Saat ini fitur edit sedang dalam pengembangan");
//
//            } else if (pesan.contains("/todo delete")) {
//                String todoName = pesan.substring(7);
//                todoService.getTodoHashMap().get(userId).remove(todoService.getTodoMap().get(todoName));
//                jawab = new TextMessage(todoName + " Berhasil dihapus");
//
//            } else if (pesan.contains("/todo debug")) {
//                jawab = new TextMessage("oke 1 2 3");
//
//            }
//
//            //
//            lineMessagingClient.replyMessage(new ReplyMessage(replyToken, jawab));
//        }
//
//        catch (NullPointerException | ExecutionException e) {
//            System.out.print("ERROR: " + e.getMessage());
//        }
//
//    }
//}
