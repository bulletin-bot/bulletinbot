package com.bot.bulletinbot.todo.service;

import com.bot.bulletinbot.todo.core.Todo;
import com.bot.bulletinbot.todo.repository.TodoRepository;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class TodoServiceImpl implements TodoService {
    private TodoRepository repo;

    public TodoServiceImpl() {
        this.repo = new TodoRepository();
        initRepo();
    }


    public Todo createTodo(String name, String userID) {
        Todo res = new Todo(name);
        return repo.add(res, userID);
    }

    public List<Todo> getTodoList() {
        return repo.getTodoList();
    }

    public HashMap<String, List> getTodoHashMap() {
        return repo.getTodoHashMap();
    }

    private void initRepo() {
        createTodo("DefaultTodo", "DefaultID");
    }
}