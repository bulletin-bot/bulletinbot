package com.bot.bulletinbot.todo.service;

import com.bot.bulletinbot.todo.core.Todo;

import java.util.List;

public interface TodoService {
    List<Todo> getTodoList();
    Todo createTodo(String name, String userID);
}
